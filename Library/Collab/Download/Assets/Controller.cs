﻿using UnityEngine;
using System.Collections;
using DigitalRuby.RainMaker;
public class Controller : MonoBehaviour
{

    private SteamVR_TrackedController device;


    [SerializeField]
    public RainScript myRain;
    [SerializeField]
    public SteamVR_LaserPointer myLaser;

    public void Start()
    {
        device = GetComponent<SteamVR_TrackedController>();
        device.TriggerClicked += Trigger;
        device.PadTouched += Pad;

    }

    public void Update()
    {
        //  myRain.RainIntensity = 0;
    }

    public void makeRain(float rainIntensity)
    {
        //float rainIntensity = myRain.RainIntensity;
        if (myRain)
            myRain.RainIntensity = rainIntensity;
    }

    void Trigger(object sender, ClickedEventArgs e)
    {
        if (myLaser)
        {
            bool laserActive = myLaser.active;
            myLaser.active = laserActive ? false : true;
        }
    }

    void Pad(object sender, ClickedEventArgs e)
    {
        float padY = e.padY;
        makeRain(padY > 0 ? padY : 0);
    }

}